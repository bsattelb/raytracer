#pragma once

#include <vector>
#include <tuple>
#include "Hitable.h"
#include "BoundingBox.h"
#include "RandomHelper.h"
#include "Sphere.h"
#include "MetaBallBVH.h"

// http://www.geisswerks.com/ryan/BLOBS/blobs.html
// https://blackpawn.com/texts/metanormals/default.html

class MetaBall : public Hitable {
public:

  std::vector<Sphere*> balls;
  BoundingBox box;

  MetaBallBVH ballBVH;

  MetaBall() {}
  MetaBall(std::vector<Sphere*> l, Material *mat) : balls(l) {
    matPtr = mat;

    BoundingBox tempBox;
    bool firstTrue = balls[0]->bounding_box(tempBox);
    box = tempBox;
    for(int i = 1; i < balls.size(); i++) {
      if(balls[i]->bounding_box(tempBox)) {
        box = BoundingBox::surrounding_box(box, tempBox);
      }
    }

    std::vector<int> indices(balls.size());
    for(int i = 0; i < balls.size(); ++i) {
      indices[i] = i;
    }
    ballBVH = MetaBallBVH(balls);
  }

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override;

  bool checkContainerHits(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const;
  double checkPotential(const Eigen::Vector3d& point, double t, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const;

  Eigen::Vector3d calcNormal(Eigen::Vector3d& point, double t, std::vector<std::tuple<bool, double, double>>& hitLocations) const;

  double checkInside(const Eigen::Vector3d& point) const;

  virtual bool bounding_box(BoundingBox& box) const;
  virtual Ray random_ray_from_surface() const;
};
