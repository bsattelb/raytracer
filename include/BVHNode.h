#pragma once
#include <eigen3/Eigen/Dense>
#include <algorithm>
#include <iostream>

#include "Hitable.h"
#include "BoundingBox.h"
#include "RandomHelper.h"

int box_x_compare(Hitable *a, Hitable *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(0) < boxRight.min(0);
}

int box_y_compare(Hitable *a, Hitable *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(1) < boxRight.min(1);
}

int box_z_compare(Hitable *a, Hitable *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(2) < boxRight.min(2);
}

class BVHNode: public Hitable {
public:
  Hitable *left;
  Hitable *right;
  BoundingBox box;

  BVHNode() {}
  BVHNode(std::vector<Hitable*> l);

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override;
  virtual bool bounding_box(BoundingBox& box) const override;
  virtual Ray random_ray_from_surface() const override;
};

BVHNode::BVHNode(std::vector<Hitable*> l) {
  int axis = int(3*RandomHelper::randomUniform());

  if(axis==0) {
    std::sort(l.begin(), l.end(), box_x_compare);
  } else if(axis==1) {
    std::sort(l.begin(), l.end(), box_y_compare);
  } else {
    std::sort(l.begin(), l.end(), box_z_compare);
  }


  if(l.size() == 1) {
    left = right = l[0];
  } else if (l.size() == 2) {
    left = l[0];
    right = l[1];
  } else {
    std::vector<Hitable*> lLeft(l.begin(), l.begin() + l.size()/2);
    std::vector<Hitable*> lRight(l.begin() + l.size()/2, l.end());
    left = new BVHNode(lLeft);
    right = new BVHNode(lRight);
  }

  BoundingBox a, b;
  if(!left->bounding_box(a) || !right->bounding_box(b)) {
    std::cerr << "No bounding box in bvh_node constructor \n";
  }

  box = BoundingBox::surrounding_box(a, b);

}

bool BVHNode::bounding_box(BoundingBox& b) const {
  b = box;
  return true;
}

bool BVHNode::hit(const Ray& r, double t_min, double t_max, HitRecord& h) const {
  if(box.hit(r, t_min, t_max)) {
    HitRecord hLeft;
    HitRecord hRight;
    bool hitLeft = left->hit(r, t_min, t_max, hLeft);
    bool hitRight = right->hit(r, t_min, t_max, hRight);

    if(hitLeft and hitRight) {
      if(hLeft.t < hRight.t) {
        h = hLeft;
      }
      else {
        h = hRight;
      }
      return true;
    }
    else if(hitLeft) {
      h = hLeft;
      return true;
    }
    else if(hitRight) {
      h = hRight;
      return true;
    }
  }
  return false;
}

Ray BVHNode::random_ray_from_surface() const {
  if(RandomHelper::randomUniform() > 0.5) {
    return left->random_ray_from_surface();
  } else {
    return right->random_ray_from_surface();
  }
}
