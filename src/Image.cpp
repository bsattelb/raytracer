#include <fstream>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <utility>
#include <vector>
#include <omp.h>
#include <algorithm>
#include <random>
#include <ctime>

#include <iostream>
#include <limits>

#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "BVHNode.h"
#include "Texture.h"
#include "Rectangle.h"
#include "MetaBall.h"
#include "PDF.h"

void Image::cornell_box(double dist) {
  std::vector<Hitable*> list;

  std::vector<Hitable*> lightList;
  Material *weirdRed = new Dielectric(2);
  Material *metal = new Metal(Eigen::Vector3d(0.9, 0.9, 0.9), 0.05);
  Material *light = new DiffuseLight(new ConstantTexture(Eigen::Vector3d(1, 1, 1)));

  Hitable *light1 = new xz_rect(-100, 100, -100, 100, 299, light);

  //Hitable *sphere1 = new Sphere(Eigen::Vector3d(300, 0, 0), 100, metal);
  //Hitable *sphere2 = new Sphere(Eigen::Vector3d(-300, 0, 0), 100, weirdRed);
  Hitable *light2 = new uni_xy_rect(-100, 100, 90, 290, -299, light, 1);

  lightList.push_back(light1);
  //list.push_back(sphere1);
  //list.push_back(sphere2);
  //lightList.push_back(light2);

  list.insert(list.end(), lightList.begin(), lightList.end());

  lights = new HitableList(lightList);

  double mix = 0.5; // 0 is only direct, 1 is only indirect


  Material *red = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.65, 0.06, 0.06)), lights, mix);
  Material *white = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), lights, mix);
  Material *green = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.45, 0.15)), lights, mix);
  Material *blue = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.012, 0.73)), lights, mix);
  Material *purple = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.012, 0.73)), lights, mix);

  Material *checkerBoard = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), new ConstantTexture(Eigen::Vector3d(0.12, 0.12, 0.12)), 0.05), lights, mix);
  Material *checkerBoard2 = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(80.0/255, 100.0/255, 0)), new ConstantTexture(Eigen::Vector3d(1, 0, 1)), 0.05), lights, mix);
  Material *marbleTexture = new Lambertian(new NoiseTexture(Eigen::Vector3d(0.73, 0.73, 0.73), 0.005), lights);
  Material *marbleTexture2 = new Lambertian(new NoiseTextureTwoColor(Eigen::Vector3d(0.73, 0.73, 0), Eigen::Vector3d(0, 0.73, 0.73), 0.005), lights, mix);
  Material *marbleTexture3 = new Lambertian(new NoiseTextureTwoColor(Eigen::Vector3d(0, 0.73, 0.73), Eigen::Vector3d(0.73, 0.73, 0), 0.005), lights, mix);

  list.push_back(new yz_rect(-300, 300, -300, 300, 600, green));
  list.push_back(new yz_rect(-300, 300, -300, 300, -600, red));
  list.push_back(new xz_rect(-600, 600, -300, 300, 300, marbleTexture3));
  list.push_back(new xz_rect(-600, 600, -300, 300, -300, marbleTexture2));

  list.push_back(new xy_rect(-600, 600, -300, 300, 300, checkerBoard2));
  list.push_back(new uni_xy_rect(-600, 600, -300, 300, -300, checkerBoard, 1));
  //


  std::vector<Sphere*> metaList;
  std::vector<Sphere*> metaList2;
  std::vector<Sphere*> metaList3;

  double baseSize = 20;
  double randomSize = 30;
  double randomDist = 50;
  double baseDist = 325;
  Eigen::Vector3d shiftVec(baseDist, 0, 0);
  int numBalls = 100;

  for(int i = 0; i < numBalls; ++i) {
    Eigen::Vector3d center(randomDist*RandomHelper::randomNormal(),
                           randomDist*RandomHelper::randomNormal(),
                           randomDist*RandomHelper::randomNormal());
    double size = randomSize*RandomHelper::randomUniform()+baseSize;
    metaList.push_back(new Sphere(center, size, metal));
    metaList2.push_back(new Sphere(center+shiftVec, size, metal));
    metaList3.push_back(new Sphere(center-shiftVec, size, metal));
  }
  metaList.push_back(new Sphere(Eigen::Vector3d(0,0,0), 3*randomSize+baseSize, metal));
  metaList2.push_back(new Sphere(Eigen::Vector3d(0,0,0)+shiftVec, 3*randomSize+baseSize, metal));
  metaList3.push_back(new Sphere(Eigen::Vector3d(0,0,0)-shiftVec, 3*randomSize+baseSize, metal));

  list.push_back(new MetaBall(metaList, white));
  list.push_back(new MetaBall(metaList2, metal));
  list.push_back(new MetaBall(metaList3, weirdRed));

  // list.push_back(new Sphere(Eigen::Vector3d(0,0,0), 100, white));
  // list.push_back(new Sphere(Eigen::Vector3d(350,0,0), 100, metal));
  // list.push_back(new Sphere(Eigen::Vector3d(-350,0,0), 100, weirdRed));

  world = new BVHNode(list);
}


Image::Image(std::string filename) {
  width = 1920;
  height = 1080;
  numSamples = 50000;
  maxDepth = 50;

  cornell_box(std::stod(filename));

  cv::namedWindow("Display Window", cv::WINDOW_AUTOSIZE);
  cv::Mat temp(height, width, CV_64FC3, cv::Scalar(0, 0, 0));
  image = temp;

  Eigen::Vector3d lookfrom(278-277.5,278-277.5,-800-277.5);
  Eigen::Vector3d lookat(278-277.5,278-277.5,-277.5);
  cam.setUp(lookfrom, lookat, Eigen::Vector3d(0,1,0), 40, double(width)/double(height), 0.0, 1.0);
}


Eigen::Vector3d Image::raytrace(Ray r) {
  Eigen::Vector3d albedo(1,1,1);
  return color(r, 0, albedo);
}

Eigen::Vector3d Image::color(Ray r, int depth, Eigen::Vector3d& albedo) {
  HitRecord h;
  if(world->hit(r, 0.001, std::numeric_limits<double>::max(), h)) {
    Ray scattered;
    double pdf = 1;
    Eigen::Vector3d attenuation;
    Eigen::Vector3d emitted = h.matPtr->emitted(h.u, h.v, h, r);
    if(depth <= maxDepth and h.matPtr->scatter(r, h, attenuation, scattered)) {
      albedo = albedo.cwiseProduct(attenuation);

      // if(albedo.maxCoeff() < RandomHelper::randomUniform()) {
      //   return albedo.cwiseProduct(emitted);
      // }
      // albedo = albedo/albedo.maxCoeff();


      // double probVal = h.matPtr->scattering_pdf(r, h, scattered)/pdf;
      // albedo = probVal*albedo;
      scattered.insideRefractive = r.insideRefractive;
      return albedo.cwiseProduct(emitted) + color(scattered, depth+1, albedo);
    } else {
      return albedo.cwiseProduct(emitted);
    }
  }
  return albedo.cwiseProduct(Eigen::Vector3d(0,0,0));
}


void Image::colorPixels() {
  Eigen::Vector3d color(0,0,0);

  std::vector<std::pair<int, int>> pixels;
  char percentDone[100] = "";

  int count = 0;
  auto start_time = time(nullptr);

  char keyPress = -1;
  int s;
  for(s = 0; s < numSamples && (keyPress != ' '); s++) {
    pixels = RandomHelper::randomizePixels(width, height);
    count = 0;
    // num_threads(7)
    #pragma omp parallel for private(color) shared(keyPress)
    for (std::pair<int, int> pixel : pixels) {
      if(keyPress != ' '){
        int i = pixel.first;
        int j = pixel.second;

        double u = double(i + RandomHelper::randomUniform())/double(width);
        double v = double(j + RandomHelper::randomUniform())/double(height);

        Ray r = cam.getPixelRay(u, v);
        color = raytrace(r);


        bool isNans = false;
        for(int i = 0; i < 3; ++i) {
          if(isnan(color(i))) {
            color(i) = 0;
            isNans = true;
          }
          if(color(i) > 1) {
            color(i) = 1;
          }
          if(color(i) < 0) {
            color(i) = 0;
           }
        }

        // OpenCV is BGR
        color.reverseInPlace();
        cv::Vec<double, 3> cvColor;
        cv::eigen2cv(color, cvColor);
        image.at<cv::Vec<double, 3>>(cv::Point(i, height - 1 - j)) += cvColor;

        count++;

        if(omp_get_thread_num() == 0 and time(nullptr) - start_time > 10) {
          double ratio = (s + double(count)/double(pixels.size()))/double(numSamples) * 100;
          snprintf(percentDone, 100, "%i/%i samples, %05.2f%% Done, ", s+1, numSamples, ratio);
          cv::setWindowTitle("Display Window", percentDone);
          cv::Mat temp;
          cv::sqrt(image/double(s+1), temp);
          //cv::pow(image/double(s+1), 1.0/5, temp);
          //temp = cv::min(temp, 1);
          //temp = cv::max(temp, 0);
          cv::imshow("Display Window", temp);
          keyPress = cv::waitKey(1);
          start_time = time(nullptr);
        }
      }
    }
  }
  std::cout << "hey" << std::endl;
  image = image/double(s+1);
  cv::sqrt(image, image);
  //cv::pow(image, 1.0/4, image );
  //cv::imshow("Display Window", image);
  //cv::setWindowTitle("Display Window", "100% Done");
  //cv::waitKey(0);
}

void Image::writeToFile(std::string filename) {
  image.convertTo(image, CV_8UC3, 255.0);
  cv::imwrite(filename, image);
}
