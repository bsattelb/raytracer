#pragma once

#include <string>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <utility>
#include <vector>
#include "Ray.h"
#include "Hitable.h"
#include "Camera.h"


class Image {
private:
  Eigen::Vector3d raytrace(Ray r);
  Eigen::Vector3d color(Ray r, int depth, Eigen::Vector3d& albedo);
  //Ray getPixelRay(int i, int j);
  Hitable *world;
  Hitable *lights;
  Camera cam;

  int width, height, numSamples, maxDepth;
  cv::Mat image;
public:
  Image(std::string filename);
  void colorPixels();
  void writeToFile(std::string filename);
  void cornell_box(double dist);
};
