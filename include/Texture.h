#pragma once

#include <eigen3/Eigen/Dense>
#include <math.h>
#include <iostream>

#include "Perlin.h"

class Texture {
public:
  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const = 0;
};

class ConstantTexture : public Texture {
public:
  Eigen::Vector3d color;
  ConstantTexture() {}
  ConstantTexture(Eigen::Vector3d c) {
    color = c;
  }

  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const override {
    return color;
  }
};

class CheckerTexture : public Texture {
public:
  Texture *odd;
  Texture *even;
  double size;

  CheckerTexture() {}
  CheckerTexture(Texture *t0, Texture *t1, double s) : even(t0), odd(t1), size(s) {}

  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const override {
    double sines = std::sin(size*p(0)) * sin(size*p(1)) * sin(size*p(2));
    if(sines < 0) {
      return odd->value(u, v, p);
    } else {
      return even->value(u, v, p);
    }
  }
};

class NoiseTexture : public Texture {
public:
  Perlin noise;
  double scale;
  Eigen::Vector3d color;

  NoiseTexture(Eigen::Vector3d c, double sc) {color=c;scale=sc;}
  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const override {
    return color*noise.turb(scale*p);
  }
};

class NoiseTextureTwoColor : public Texture {
public:
  Perlin noise;
  double scale;
  Eigen::Vector3d color;
  Eigen::Vector3d color2;

  NoiseTextureTwoColor(Eigen::Vector3d c, Eigen::Vector3d c2, double sc) {color=c;color2=c2;scale=sc;}
  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const override {
    double n = std::sqrt(noise.turb(scale*p));
    return color*n + (1-n)*color2;
  }
};

class MarbleTexture : public Texture {
public:
  Perlin noise;
  double scale;
  Eigen::Vector3d color;

  MarbleTexture(Eigen::Vector3d c, double sc) {color=c, scale=sc;}
  virtual Eigen::Vector3d value(double u, double v, const Eigen::Vector3d& p) const override {
    return color*0.5*(1 + std::sin(scale*p(2) + 10*noise.turb(p)));
  }
};
