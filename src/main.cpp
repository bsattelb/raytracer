#include <eigen3/Eigen/Dense>
#include <iostream>

#include "Image.h"

int main(int argc, char* argv[]) {
  if(argc < 2) {
    std::cout << "Not enough file names entered.  Exiting.";
    return -1;
  }

  Image image(argv[1]);
  image.colorPixels();
  image.writeToFile(argv[2]);

  return 0;
}
