#pragma once

#include <random>
#include <vector>
#include <utility>
#include <eigen3/Eigen/Dense>

class RandomHelper {
public:
  static std::default_random_engine rng;
  static std::default_random_engine seededRNG;

  static std::vector<std::pair<int, int>> randomizePixels(int width, int height) {
    std::vector<std::pair<int, int>> pixels;
    for(int j = height-1; j >= 0; j--) {
      for(int i = 0; i < width; i++) {
        std::pair<int, int> pixel(i, j);
        pixels.push_back(pixel);
      }
    }
    std::shuffle(std::begin(pixels), std::end(pixels), rng);
    return pixels;
  }

  static double randomUniform() {
    static std::uniform_real_distribution<double> distr(0, 1);
    return distr(rng);
  }

  static double randomSeededUniform() {
    static std::uniform_real_distribution<double> distr(0, 1);
    return distr(seededRNG);
  }

  static double randomNormal() {
    static std::normal_distribution<double> distr(0, 1);
    return distr(rng);
  }

  static double randomInt(int start, int end) {
    std::uniform_int_distribution<int> distr(start, end);
    return distr(rng);
  }

  static Eigen::Vector3d randomInUnitSphere() {
    Eigen::Vector3d p;
    do {
      p = 2*Eigen::Vector3d(randomUniform(), randomUniform(), randomUniform()) - Eigen::Vector3d(1,1,1);
    } while(p.dot(p) >= 1);
    return p;
  }

  static Eigen::Vector3d randomOnUnitSphere() {
    Eigen::Vector3d p(randomNormal(), randomNormal(), randomNormal());
    p.normalize();
    return p;
  }

  static Eigen::Vector3d randomInUnitDisk() {
    Eigen::Vector3d p;
    do {
      p = 2*Eigen::Vector3d(randomUniform(), randomUniform(), 0) - Eigen::Vector3d(1,1,0);
    } while(p.dot(p) >= 1);
    return p;
  }
};
