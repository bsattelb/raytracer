#pragma once
#include <eigen3/Eigen/Dense>
#include <algorithm>
#include <iostream>
#include <tuple>
#include <vector>

#include "BoundingBox.h"
#include "RandomHelper.h"

class MetaBallBVHNode {
public:
  virtual bool hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const = 0;
  virtual bool bounding_box(BoundingBox& b) const = 0;
};

class MetaBallBVH {
public:
  MetaBallBVHNode *node;

  MetaBallBVH() {}
  MetaBallBVH(std::vector<Sphere*> spheres);

  bool hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const;
};



class SphereIndex : public MetaBallBVHNode {
public:
  Sphere* sphere;
  int index;

  SphereIndex() {}
  SphereIndex(Sphere* sphere, int index) : sphere(sphere), index(index) {}
  bool hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const override;
  bool bounding_box(BoundingBox& b) const override {return sphere->bounding_box(b);}
};



class MetaBallBVHContainer : public MetaBallBVHNode {
public:
  MetaBallBVHNode *left;
  MetaBallBVHNode *right;
  BoundingBox box;

  MetaBallBVHContainer() {}
  MetaBallBVHContainer(std::vector<SphereIndex*> s);

  bool hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const override;

  bool bounding_box(BoundingBox& b) const override{
    b = box;
    return true;
  }
};
