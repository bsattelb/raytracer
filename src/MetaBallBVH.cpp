#include <eigen3/Eigen/Dense>
#include <algorithm>
#include <iostream>
#include <tuple>
#include <vector>

#include "Sphere.h"
#include "Ray.h"
#include "BoundingBox.h"
#include "RandomHelper.h"
#include "MetaBallBVH.h"

MetaBallBVH::MetaBallBVH(std::vector<Sphere*> spheres) {
  std::vector<SphereIndex*> s;

  for(int i = 0; i < spheres.size(); ++i) {
    s.push_back(new SphereIndex(spheres[i], i));
  }

  node = new MetaBallBVHContainer(s);
}

bool MetaBallBVH::hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  return node->hit(r, t_min, t_max, hitLocations);
}

bool SphereIndex::hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  Eigen::Vector2d tVals;
  bool didHit = sphere->getTvals(r, tVals);
  hitLocations[index] = std::tuple<bool, double, double>(didHit, tVals[0], tVals[1]);
  return didHit;
}

int box_x_compare_metaball(MetaBallBVHNode *a, MetaBallBVHNode *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(0) < boxRight.min(0);
}
int box_y_compare_metaball(MetaBallBVHNode *a, MetaBallBVHNode *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(1) < boxRight.min(1);
}
int box_z_compare_metaball(MetaBallBVHNode *a, MetaBallBVHNode *b) {
  BoundingBox boxLeft, boxRight;
  if(!a->bounding_box(boxLeft) or !b->bounding_box(boxRight)) {
    std::cerr << "No bounding box in bvh_node constructor\n";
  }
  return boxLeft.min(2) < boxRight.min(2);
}

MetaBallBVHContainer::MetaBallBVHContainer(std::vector<SphereIndex*> s) {
  int axis = int(3*RandomHelper::randomUniform());
  
  if(axis==0) {
    std::sort(s.begin(), s.end(), box_x_compare_metaball);
  } else if(axis==1) {
    std::sort(s.begin(), s.end(), box_y_compare_metaball);
  } else {
    std::sort(s.begin(), s.end(), box_z_compare_metaball);
  }

  if(s.size() == 1) {
    left = right = s[0];
  } else if (s.size() == 2) {
    left = s[0];
    right = s[1];
  } else {
    std::vector<SphereIndex*> sLeft(s.begin(), s.begin() + s.size()/2);
    std::vector<SphereIndex*> sRight(s.begin() + s.size()/2, s.end());
    left = new MetaBallBVHContainer(sLeft);
    right = new MetaBallBVHContainer(sRight);
  }
  BoundingBox a, b;
  if(!left->bounding_box(a) || !right->bounding_box(b)) {
    std::cerr << "No bounding box in bvh_node constructor \n";
  }
  box = BoundingBox::surrounding_box(a, b);
}

bool MetaBallBVHContainer::hit(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  if(box.hit(r, t_min, t_max)) {
    bool hitLeft = left->hit(r, t_min, t_max, hitLocations);
    bool hitRight = right->hit(r, t_min, t_max, hitLocations);
    return hitLeft || hitRight;
  }
  return false;
}
