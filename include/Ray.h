#pragma once
#include <eigen3/Eigen/Dense>

class Ray {
public:
  Eigen::Vector3d origin, direction;
  mutable bool insideRefractive;
  Ray() {};
  Ray(const Eigen::Vector3d loc, const Eigen::Vector3d dir) {
    origin = loc;
    direction = dir.normalized();
    insideRefractive=false;
  }

  Eigen::Vector3d pointAt(double t) const {
    return origin + t*direction;
  }
};
